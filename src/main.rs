#![allow(dead_code)]

// This is a poll driver that provides a push interface while honouring hysteresis of an averaged value

extern crate serial;
extern crate ascii;

use std::str;
use std::{thread};
use std::time::{Duration, Instant};

use std::io::prelude::*;
use serial::prelude::*;

use rumqtt::{MqttClient, MqttOptions, QoS};
use gpio_cdev::{Chip, LineRequestFlags, LineHandle};
use serial::{SystemPort};

// MQTT basics
const MQTT_TOPIC : &str = "abiyo/sensor/waterlevel";
const MQTT_SERVER_IP : &str = "10.0.0.54";
const MQTT_SERVER_PORT : u16 = 1883;
const MQTT_CLIENT_ID : &str = "abiyo-sensor-waterlevel";

// Serial connection and boundaries to collect the payload bytes
const SERIAL_PORT : &str = "/dev/ttyTHS1";
const START_TOKEN : &[u8] = b"val=";
const END_TOKEN : &[u8] = b"\xff\xff\xff";
const MAX_RANGE_MESSAGE_SIZE : usize = 32;

// Gpio definition for trigger input
const GPIO_CHIP : &str = "/dev/gpiochip0";
const GPIO_LINE_NUMBER : u32 = 216;

// Smooth publishing and reducing data volumen
const PUBLISHING_HYSTERESIS : u32 = 1;
const AVG_WINDOW : u32 = 4;
const MIN_VALID_RANGE_VALUE : u32 = 23;

fn dump(data : &Vec<u8>) {
    print!("-> dump: ");
    for i in data {

        match ascii::AsciiChar::from_ascii(*i) {
            Ok(ch) => print!("{}", ch),
            Err(_) => print!("_0x{:02x}", *i)
        };
    }

    println!("");
}

fn setup_serial_port() -> SystemPort {
    let mut port = serial::open(SERIAL_PORT).unwrap();

    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud9600)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }).unwrap();

    port.set_timeout(Duration::from_millis(1000)).unwrap();
    
    return port;
}

fn setup_gpio() -> LineHandle {
    let mut chip = Chip::new(GPIO_CHIP).unwrap();
    let output = chip.get_line(GPIO_LINE_NUMBER).unwrap();
    let output_handle = output.request(LineRequestFlags::OUTPUT, 0, MQTT_CLIENT_ID).unwrap();
    
    return output_handle;
}

fn send_trigger_pulse(handle : &LineHandle) {
    handle.set_value(1).unwrap();
    let pulse_width = Duration::from_millis(1);
    thread::sleep(pulse_width);
    handle.set_value(0).unwrap();
}

fn take_measurement(handle : &LineHandle, port : &mut SystemPort) -> u32 {
    let mut rx_buf: Vec<u8> = vec![0u8; MAX_RANGE_MESSAGE_SIZE];

    // Trigger a measurement
    send_trigger_pulse(&handle);

    // Get a chunk of data
    port.read(&mut rx_buf).expect("Cannot read from serial port");
    //dump(&rx_buf);

    // Find start of payload
    let start_index = rx_buf.windows(START_TOKEN.len()).position(|x| {return x == START_TOKEN;}).expect("Cannot find start token");
    let first_index_of_interest = start_index + START_TOKEN.len();

    // Find end of payload
    let end_index_of_substring = rx_buf[first_index_of_interest..].windows(END_TOKEN.len()).position(|x| {return x == END_TOKEN;}).expect("Cannot find end token");
    let last_index_of_interest = end_index_of_substring + first_index_of_interest;

    // Extract payload
    let payload = str::from_utf8(&rx_buf[first_index_of_interest..last_index_of_interest]).expect("Error in parsing extracted payload");

    let measurement_mm = payload.parse::<u32>().expect("Cannot parse payload string into number");
    return measurement_mm;
}


fn main() {
    let mqtt_options = MqttOptions::new(MQTT_CLIENT_ID, MQTT_SERVER_IP, MQTT_SERVER_PORT);
    let (mut mqtt_client, _notifications) = MqttClient::start(mqtt_options).unwrap();

    let mut port = setup_serial_port();
    let handle = setup_gpio();

    // State is tracked to generate events that are being published
    let mut known_state : Option<u32> = None;

    // Keep track of the number of measurements done
    // A measurement is an averaged value
    let mut num_measurements = 0u32;

    // Used for unaveraged sensor sampling frequency calculation
    let mut start = Instant::now();

    loop {
        num_measurements = num_measurements + 1;

        let mut numbers : Vec<u32> = Vec::new();

        // Collect data for averaging
        for _i in 0..AVG_WINDOW {
            numbers.push(take_measurement(&handle, &mut port));
        }
        let avged_raw_measurement = numbers.iter().sum::<u32>() as f64 / numbers.len() as f64;

        // Declare offical measured state
        let current_state = avged_raw_measurement as u32;

        // Check if it is time for a console debug update message
        const OBSERVATION_DURATION : u32 = 100;
        if num_measurements % OBSERVATION_DURATION == 0 {
            print!("Raw {} mm", avged_raw_measurement);

            // Adjust elapsed time for observation duration and avg-window and convert to frequency
            let duration = 1f32 / (start.elapsed().as_secs_f32() / (OBSERVATION_DURATION * AVG_WINDOW) as f32);
            start = Instant::now();
            println!("sampling rate: {:?} hz", duration);
        }

        if current_state < MIN_VALID_RANGE_VALUE {
            println!("Discarding");
            continue;
        }

        // Main logic
        known_state = match known_state {
            Some(s) => {
                // Compare new value to old value
                let mut delta : u32 = 0;

                if current_state > s {
                    delta = current_state - s;
                } else if current_state < s {
                    delta = s - current_state;
                }

                // Check for relevant change in value and publish
                if delta > PUBLISHING_HYSTERESIS {
                    println!("New state is {} mm", current_state);
                    mqtt_client.publish(MQTT_TOPIC, QoS::AtLeastOnce, false, format!("{}", current_state)).expect("Error publishing");
                    Some(current_state)    
                } else {
                    // Silently stick with old value if change is not relevant enough
                    Some(s)
                }
            },
            None => {
                // We have got a very first value
                println!("Initial state is {} mm", current_state);

                // Publish this right away
                mqtt_client.publish(MQTT_TOPIC, QoS::AtLeastOnce, false, format!("{}", current_state)).expect("Error publishing");
                Some(current_state)
            }
        };

        let delay = Duration::new(0, 100);
        thread::sleep(delay);
    };

    /*
    mqtt_client.subscribe("hello/world", QoS::AtLeastOnce).unwrap();
    let sleep_time = Duration::from_secs(1);
    for notification in notifications {
        println!("{:?}", notification)
    }

    thread::spawn(move || {
        for i in 0..10000 {
        }
    });

    */
}
